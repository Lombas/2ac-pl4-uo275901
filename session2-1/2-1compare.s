.data

   str1:  .asciiz "whatever1" ; 9 chars
   str2:  .asciiz "whatever2" ; 9 chars
   result: .word 0

.code

main:
   xor r10, r10, r10 ;Counter, acts as index

   ; Loop through string until \0
loop1:
   lbu r8, str1(r10)
   beqz r8, end1 ; If end of string, jump to end
   daddui r10, r10, 1
   j loop1 ; Restart loop

end1:
   xor r11, r11, r11

loop2:
   lbu r8, str2(r11)
   beqz r8, end2 ; If end of string, jump to end
   daddui r11, r11, 1
   j loop2 ; Restart loop

end2:
   xor r13, r13, r13
   beq r11, r10, equal
   j end

equal:
   daddui r13, r13, 1
   j end

end:
   sd r13, result(r0)
   halt

