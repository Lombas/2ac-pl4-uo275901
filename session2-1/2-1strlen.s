.data

   str:  .asciiz "whatever1" ; 9 chars
   result: .word 0

.code

main:
   xor r10, r10, r10 ;Counter, acts as index

   ; Loop through string until \0
loop:
   lbu r8, str(r10)
   beqz r8, end ; If end of string, jump to end
   daddui r10, r10, 1
   j loop ; Restart loop

end:
   sd r10, result(r0)
   halt
