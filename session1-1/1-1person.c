#include <stdio.h>
#include <string.h>

typedef struct
{
	char name[30];
	int heightcm;
	double weightkg;
} Person;

typedef Person* PPerson;

//I dont add the argc or argv as the program doesnt use them
//and it feels cleaner this way
int main()
{
	Person javier;
	PPerson pJavier = &javier;

	strcpy(javier.name, "Javier");
	javier.heightcm = 175;
	javier.weightkg = 95.0f;

	pJavier->weightkg = 67.0f;

	printf("%1$s's height: %2$i cm; %1$s's weight: %3$.1f kg\n", javier.name, javier.heightcm, javier.weightkg);
}
