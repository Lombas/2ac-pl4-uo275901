#include <stdio.h>

int main()
{
	int i = 3;
	float f = 2.4f;
	char *s = "Content";

	printf("i content: %i\n", i);
	printf("f content: %.6f\n", f);
	printf("s content: %s\n", s);

	return 0;
}
